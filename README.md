# gym-xplane

An OpenAI Gym Environment for X-Plane

## Installation
```bash
cd gym-xplane
pip install -e .
```
