import gym
from gym import error, spaces, utils
from gym.utils import seeding

class XPlaneDifficultEnv(gym.Env):
  metadata = {'render.modes': ['human']}

  def __init__(self):
    # init
  def step(self, action):
    # step
  def reset(self):
    # reset
  def render(self, mode='human'):
    # render
  def close(self):
    # close
