import gym
from gym import error, spaces, utils
from gym.utils import seeding
from .shared import xpc
import numpy as np

class XPlaneEnv(gym.Env):
    metadata = {'render.modes': ['human']}
    client = None

    aileron_hist = []
    elevator_hist = []
    rudder_hist = []
    throttle_hist = []

    intended_lat_hist = []
    intended_lon_hist = []
    intended_x_hist = []
    intended_y_hist = []
    intended_altitude_hist = []

    actual_lat_hist = []
    actual_lon_hist = []
    actual_x_hist = []
    actual_y_hist = []
    actual_altitude_hist = []

    disp_lat_hist = []
    disp_lon_hist = []
    disp_x_hist = []
    disp_y_hist = []
    disp_altitude_hist = []

    pitch_hist = []
    roll_hist = []
    yaw_hist = []

    airspeed_hist = []
    downward_gs_hist = []

    def __init__(self):
        self.client = xpc.XPlaneConnect()
        try:
            client.getDREF("sim/test/test_float")
        except:
            print("Error establishing connection to X-Plane.")
            print("Exiting...")

        center_lat = 40.273502
        center_lon = -86.126976
        altitude = 2500

        # generate random start point
        # choose random direction

        posi = [37.524, -122.06899, 2500, 0, 0, 0, 1]
        client.sendPOSI(posi)

        data = [
            [18, 0, -998, 0, -998, -998, -998, -998, -998],
            [3, 130, 130, 130, 130, -998, -998, -998, -998],
            [16, 0, 0, 0, -998, -998, -998, -998, -998]
        ]
        client.sendDATA(data)

        ctrl = [0.0, 0.0, 0.0, 0.0]
        client.sendCTRL(ctrl)

        client.pauseSim(True)

    def step(self, action):

        # step
    def reset(self):
        # reset
    def render(self, mode='human'):
        # render
    def close(self):
        # close

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
