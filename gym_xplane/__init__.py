from gym.envs.registration import register

register(
    id='xplane-v0',
    entry_point='gym_xplane.envs:XPlaneEnv',
)
register(
    id='xplane-difficult-v0',
    entry_point='gym_xplane.envs:XPlaneDifficultEnv',
)
